# libCharon
File metadata and streaming library

## Documentation
- [Library](docs/library.md)
- [UFP](docs/ultimaker_format_package.md)
- [Service](docs/service.md)
